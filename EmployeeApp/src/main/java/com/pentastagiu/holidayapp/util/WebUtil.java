package com.pentastagiu.holidayapp.util;

import java.io.StringWriter;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Different methods used in web pages
 */
@ManagedBean(name = "webUtil")
@SessionScoped
public class WebUtil {
    /**
     * Sets the language for the application
     * 
     * @param the
     *            request from the page
     * @return a bundle with the requested language
     */
    public ResourceBundle setLanguage(HttpServletRequest request) {
        HttpSession userSession = request.getSession();
        String locale = request.getParameter("locale");
        Locale preferredLocale = null;
        // language is English by default
        if (userSession.isNew()) {
            locale = "en";
            preferredLocale = new Locale("en", "EN");
            userSession.setAttribute("Locale", locale);
        } else {
            // user option selection
            if (locale != null) {
                if (locale.equals("en")) {
                    preferredLocale = new Locale("en", "EN");
                    userSession.setAttribute("Locale", locale);
                } else {
                    preferredLocale = new Locale("ro", "RO");
                    userSession.setAttribute("Locale", locale);
                }
                // take language from session
            } else {
                if (preferredLocale == null) {
                    if (userSession.getAttribute("Locale").equals("en")) {
                        preferredLocale = new Locale("en", "EN");
                    } else {
                        preferredLocale = new Locale("ro", "RO");
                    }
                }
            }
        }
        ResourceBundle bundle = ResourceBundle.getBundle(
                "com.pentastagiu.holidayapp.i18n.MessagesBundle_"
                        + userSession.getAttribute("Locale"), preferredLocale);
        return bundle;
    }

    /**
     * @return an html menu
     */
    public String createMenu(ResourceBundle messages) {
        String menu = null;
        menu = "<div id='menu'><ul>"
                + "<li><a href='/EmployeeApp/employeeservlet'>"
                + messages.getString("menutab1")
                + "</a></li>"
                + "<li><a href='/EmployeeApp/departmentsservlet'>"
                + messages.getString("menutab2")
                + "</a></li>"
                + "<li><a href='/EmployeeApp/HolidayFacesServlet.xhtml'>"
                + messages.getString("menutab3")
                + "</a></li>"
                + "<li><a href='/EmployeeApp/searchservlet'>"
                + messages.getString("menutab4")
                + "</a></li>"
                + "</div><br><br><script type='text/javascript'>setActive();</script>";
        return menu;
    }

    /**
     * @param buttonName
     * @return an html button
     */
    public String createAddButton(String buttonName,ResourceBundle messages) {
        String button = null;
        if (messages.getString("Addemployee").equals(buttonName)) {
            button = "<a href='/EmployeeApp/addemployeeservlet' class='addButton'>"
                    + buttonName + "</a>";
        } else if (messages.getString("Addholiday").equals(buttonName)) {
            button = "<a href='/EmployeeApp/addholidayservlet' class='addButton'>"
                    + buttonName + "</a>";
        } else if (messages.getString("Adddepartment").equals(buttonName)) {
            button = "<a href='/EmployeeApp/adddepartmentservlet' class='addButton'>"
                    + buttonName + "</a>";
        }
        return button;
    }

    public String printPageNavigation(String url, Integer nrPages,
            HttpServletRequest request) {
        String menu = null;
        int currentPage;
        if (request.getParameter("pageIndex") != null) {
            currentPage = Integer.parseInt(request.getParameter("pageIndex"));
        } else {
            currentPage = 1;
        }
        menu = "<div class='pageNumbers'>" + "<ul class='pagination'>"
                + "<li><a href='" + url + "?pageIndex=1'>" + "<<" + "</a></li>";
        if (currentPage - 1 > 0) {
            menu = menu + "<li><a href='" + url + "?pageIndex="
                    + (currentPage - 1) + "'>" + "<" + "</a></li>";
        } else {
            menu = menu + "<li><a href='" + url + "?pageIndex=" + currentPage
                    + "'>" + "<" + "</a></li>";
        }

        for (int i = 1; i <= nrPages; i++) {
            menu = menu + "<li><a href='" + url + "?pageIndex=" + i + "'>" + i
                    + "</a></li>";

        }
        if (currentPage + 1 <= nrPages) {
            menu = menu + "<li><a href='" + url + "?pageIndex="
                    + (currentPage + 1) + "'>" + ">" + "</a></li>"
                    + "<li><a href='" + url + "?pageIndex=" + nrPages + "'>"
                    + ">>" + "</a></li>";

        } else {
            menu = menu + "<li><a href='" + url + "?pageIndex=" + currentPage
                    + "'>" + ">" + "</a></li>" + "<li><a href='" + url
                    + "?pageIndex=" + nrPages + "'>" + ">>" + "</a></li>";
        }
        menu = menu + "</ul></div>";
        return menu;
    }

    /**
     * @return an html header for Employee table
     */
    public String tableHeaderEmployee(ResourceBundle messages) {
        String menuTable = null;
        menuTable = "<TH align=center> " + "id" + " </TH>"
                + "<TH align=center> " + messages.getString("empName")
                + " </TH>" + "<TH align=center> "
                + messages.getString("empHireDate") + " </TH>"
                + "<TH align=center> " + messages.getString("manager")
                + " </TH>" + "<TH align=center> " + messages.getString("dept")
                + " </TH>" + "<TH align=center> "
                + messages.getString("update") + " </TH>";
        return menuTable;
    }

    /**
     * @return an html header for Holiday table
     */
    public String tableHeaderHoliday(ResourceBundle messages) {
        String menuTable = null;
        menuTable = "<TH align=center> " + "id" + " </TH>"
                + "<TH align=center> " + messages.getString("employee")
                + " </TH>" + "<TH align=center> "
                + messages.getString("manager") + " </TH>"
                + "<TH align=center> " + messages.getString("period")
                + " </TH>" + "<TH align=center> "
                + messages.getString("request") + " </TH>"
                + "<TH align=center> " + messages.getString("approval")
                + " </TH>" + "<TH align=center> "
                + messages.getString("update") + " </TH>";
        return menuTable;
    }

    /**
     * @return an html header for Department table
     */
    public String tableHeaderDepartment(ResourceBundle messages) {
        String menuTable = null;
        menuTable = "<TH align=center> " + "id" + " </TH>"
                + "<TH align=center> " + messages.getString("deptName")
                + " </TH>" + "<TH align=center> "
                + messages.getString("description") + " </TH>"
                + "<TH align=center> " + messages.getString("updateDept")
                + " </TH>";
        return menuTable;
    }

    /**
     * @param title
     * @return an html header for web application
     */
    public String createHtmlHeader(String title) {

        String htmlHeader = null;
        htmlHeader = "<!DOCTYPE html><HTML lang='en'><HEAD>"
                + "<script type='text/javascript' src='script.js'></script><style> @import url(style.css); </style>"
                + "<meta charset='utf-8'>"
                + "<meta name='viewport' content='width=device-width, initial-scale=1'>"
                +"<link rel='shortcut icon' href='favicon.ico' type='image/x-icon'>"
                + "<link rel='stylesheet' href='bootstrap/css/bootstrap.min.css'>"
                + "<script src='bootstrap/js/1.11.1-jquery.min.js'></script>"
                + "<script src='bootstrap/js/bootstrap.min.js'></script><TITLE> "
                + title
                + "</TITLE></HEAD><BODY>"
                + "<div class='header'>"
                + "<h1><img src='holidaylogo.gif' alt='some text' id='logo'>Employees Holidays</h1></div>"
                + "<form   method='get' action=''>"
                + "<div class='langForm'><button  type='image' name='locale' value='en' id='langButton'><img src='en.png' class='langImg'></button>"
                + "<button  type='image' name='locale' value='ro' id='langButton'><img src='ro.png' class='langImg'></button>"
                + "</div></form>";
        return htmlHeader;
    }

    public String getHtmlFooter() {
        String htmlFooter = "<div id='footer'><p>&#169 Pentalog Evolution Program</p></div></BODY></HTML>";
        return htmlFooter;
    }

    public String getHead(int level, String heading) {
        return "<H" + level + "> " + heading + "</H" + level + ">";
    }

    public String getTableHead(String align, int border) {

        String tableHeader = null;
        tableHeader = "<TABLE align=" + align + " border=" + border + ">";
        return tableHeader;

    }

    public String getTR(String align) {
        String TRCell = null;
        TRCell = "<TR align=" + align + ">";
        return TRCell;
    }

    public String getTR() {
        String TRCell = null;
        TRCell = "<TR>";
        return TRCell;
    }

    public String getTD(String align, String value) {
        String TDCell = null;
        TDCell = "<TD align=" + align + "> " + value + " </TD>";
        return TDCell;
    }

    public String getTD() {
        String TDCell = null;
        TDCell = "<TD>";
        return TDCell;
    }

    public String getTD(int width) {
        String TDCell = null;
        TDCell = "<TD WIDTH=" + width + ">";
        return TDCell;
    }

    public String getTH(String align, String value) {
        String THCell = null;
        THCell = "<TH align=" + align + "> " + value + " </TH>";
        return THCell;
    }

    public String getClosedTR() {
        String TRCell = null;
        TRCell = "</TR>";
        return TRCell;
    }

    public String getClosedTD() {
        String TDCell = null;
        TDCell = "</TD>";
        return TDCell;
    }

    public String getBR(int lines) {

        StringWriter lineBR = new StringWriter();
        String lineBRs = new String();

        for (int i = 0; i <= lines; i++) {
            lineBR.write("<BR>\n");
        }
        lineBRs = lineBR.toString();

        return lineBRs;
    }

    public String getLI(String item) {

        String li = new String("<LI>");
        li += item;
        return li;
    }
}
package com.pentastagiu.holidayapp;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.inject.Inject;
import javax.inject.Named;

import com.pentastagiu.holidayapp.model.UserRoles;
import com.pentastagiu.holidayapp.model.Users;
import com.pentastagiu.holidayapp.service.UsersService;

/**
 * uploads the data base with some default values
 */
@Named(value = "dbinitializer")
public class DBInitialiser {
	@Inject
	private UsersService addServ;

	/**
	 * insert 3 Employee objects in data base
	 * 
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */

	public void initiateUsers() throws NoSuchAlgorithmException,
			UnsupportedEncodingException {
		String password = "ionut";
		String userName = "ionut";
		/*
		 * EmployeeService addServ = new EmployeeService(); DepartmentService
		 * depServ = new DepartmentService(); SimpleDateFormat dateFormat = new
		 * SimpleDateFormat("yyyy/MM/dd");
		 * 
		 * try { Date hiredate1 = dateFormat.parse(new String("2010/03/05"));
		 * Date hiredate2 = dateFormat.parse(new String("2014/01/13")); Date
		 * hiredate3 = dateFormat.parse(new String("2012/08/05"));
		 * 
		 * List<Department> departList = depServ.getDepartmentsDao()
		 * .getAllDepts(); Department depart =
		 * depServ.getDepartmentById(departList, 2);
		 * 
		 * addServ.addEmployeeDao(new Employee("Andrew", "Garcia", hiredate1,
		 * "Florin", depart)); addServ.addEmployeeDao(new Employee("Robert",
		 * "Miller", hiredate2, "Florin", depart)); addServ.addEmployeeDao(new
		 * Employee("Jones", "Wilson", hiredate3, "Florin", depart));
		 * 
		 * } catch (ParseException e) { e.printStackTrace(); }
		 */
		MessageDigest md;
		String salt = "a1s2d3f4g5h6j7k8";
		String token = new String();
		try {
			md = MessageDigest.getInstance("MD5");
			md.update((password.concat(userName).concat(salt)).getBytes());
			byte[] mdBytes = md.digest();
			StringBuffer tokenBuffer = new StringBuffer();
			for (int i = 0; i < mdBytes.length; i++) {
				tokenBuffer.append(Integer.toString(
						(mdBytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			token = tokenBuffer.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		UserRoles role = new UserRoles("admin");
		addServ.addUserDao(new Users(userName, token, "a1s2d3f4g5h6j7k8", role));
	}
}

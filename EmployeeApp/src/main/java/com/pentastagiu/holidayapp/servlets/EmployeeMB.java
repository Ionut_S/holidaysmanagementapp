package com.pentastagiu.holidayapp.servlets;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;

import com.pentastagiu.holidayapp.model.Department;
import com.pentastagiu.holidayapp.model.Employee;
import com.pentastagiu.holidayapp.model.Users;
import com.pentastagiu.holidayapp.service.DepartmentService;
import com.pentastagiu.holidayapp.service.EmployeeService;
import com.pentastagiu.holidayapp.service.LoginService;

@ManagedBean(name = "employeeMB")
@ViewScoped
public class EmployeeMB implements Serializable {

	private static final long serialVersionUID = 1L;
	@ManagedProperty("#{departmentService}")
	private DepartmentService depServ;
	@ManagedProperty("#{employeeService}")
	private EmployeeService empServ;
	@ManagedProperty("#{loginService}")
	private LoginService loginServ;
	@ManagedProperty(value = "#{param.employeeId}")
	private Integer id;
	private String firstName;
	private String lastName;
	private String cnp;
	private String date;
	private String mng;
	private String depart;
	List<Employee> listEmployee;
	List<Employee> listManager;
	Employee employee;
	Employee manager;
	Employee editEmp;
	Department department;
	private String searchString;
	private Employee selectedEmployee;
	private Users currentUser;

	public List<Employee> getListManager() {
		return listManager;
	}

	public void setCurrentUser(Users cUser) {
		this.currentUser = cUser;
	}

	public Users getCurrentUser() {
		return this.currentUser;
	}

	public void setLoginServ(LoginService loginServ) {
		this.loginServ = loginServ;
	}

	public LoginService getLoginService() {
		return this.loginServ;
	}

	public void setListManager(List<Employee> listManager) {
		this.listManager = listManager;
	}

	List<Department> listDepartment;

	public List<Department> getListDepartment() {
		return listDepartment;
	}

	public void setListDepartment(List<Department> listDepartment) {
		this.listDepartment = listDepartment;
	}

	public Employee getSelectedEmployee() {
		return selectedEmployee;
	}

	public void setSelectedEmployee(Employee selectedEmployee) {
		this.selectedEmployee = selectedEmployee;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public EmployeeMB() {
		employee = new Employee();
		editEmp = new Employee();

	}

	public Employee getEditEmp() {
		return editEmp;
	}

	public void setEditEmp(Employee editEmp) {
		this.editEmp = editEmp;
	}

	@PostConstruct
	public void init() {
		listEmployee = empServ.getAllEmployees();
		listDepartment = depServ.getAllDepartments();
		HttpServletRequest req = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		HttpSession ses = req.getSession(false);
		String name = (String) ses.getAttribute("username");
		this.currentUser = loginServ.getUser(name);
	}

	public void reset() {
		RequestContext.getCurrentInstance().reset("form:panel");
	}

	public void addEmployee() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date hireDate = null;

		try {
			hireDate = dateFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		manager = empServ.getEmployeeByName(listEmployee, firstName);
		department = depServ.getDepartmentByName(listDepartment, depart);
		manager = empServ.getEmployeeByName(listEmployee, mng);
		employee = new Employee(firstName, lastName, cnp, hireDate, manager,
				department);
		empServ.addEmployeeDao(employee);
		ExternalContext context = FacesContext.getCurrentInstance()
				.getExternalContext();
		try {
			context.redirect("EmployeeFacesServlet.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public String edit() {
		this.editEmp = empServ.getEmployeeById(listEmployee, id);
		this.editEmp.setId(id);
		listManager = empServ.getManagerList(listEmployee);
		date = editEmp.convertTime(editEmp.getHiredate());
		mng = editEmp.getManager().getFirstName();
		depart = editEmp.getDept().getName();

		return "/EditEmployeeFacesServlet.xhtml";
	}

	public void update() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date hireDate = null;
		try {
			hireDate = dateFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.editEmp.setId(id);
		department = depServ.getDepartmentByName(listDepartment, depart);
		manager = empServ.getEmployeeByName(listEmployee, mng);
		this.editEmp.setDept(department);
		this.editEmp.setManager(manager);
		this.editEmp.setHiredate(hireDate);
		empServ.updEmp(this.editEmp);
		ExternalContext context = FacesContext.getCurrentInstance()
				.getExternalContext();
		try {
			context.redirect("EmployeeFacesServlet.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void onCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("Item Cancelled");
		FacesContext.getCurrentInstance().addMessage(null, msg);
		listEmployee.remove((Employee) event.getObject());
	}

	public void saveAction() {

		// get all existing value but set "editable" to false
		for (Employee currentEmp : listEmployee) {
			currentEmp.setEditable(false);
		}

	}

	public void editAction(Employee employee) {

		employee.setEditable(true);

	}

	public List<Employee> getAllEmployees() {
		return empServ.getAllEmployees();
	}

	public DepartmentService getDepServ() {
		return depServ;
	}

	public void setDepServ(DepartmentService depServ) {
		this.depServ = depServ;
	}

	public EmployeeService getEmpServ() {
		return empServ;
	}

	public void setEmpServ(EmployeeService empServ) {
		this.empServ = empServ;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMng() {
		return mng;
	}

	public void setMng(String mng) {
		this.mng = mng;
	}

	public String getDepart() {
		return depart;
	}

	public void setDepart(String depart) {
		this.depart = depart;
	}

	public List<Employee> getListEmployee() {
		return listEmployee;
	}

	public void setListEmployee(List<Employee> listEmployee) {
		this.listEmployee = listEmployee;
	}

	public Employee getEmp() {
		return employee;
	}

	public void setEmp(Employee emp) {
		this.employee = emp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getCnp() {
		return this.cnp;
	}

	public void search(AjaxBehaviorEvent event) {
		System.out.println("search: " + this.searchString);
	}

	public Employee searchEmployeeByName() {
		return empServ.getEmployeeByName(listEmployee, searchString);
	}
}

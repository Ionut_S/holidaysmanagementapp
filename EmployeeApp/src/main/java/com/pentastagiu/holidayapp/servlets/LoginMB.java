package com.pentastagiu.holidayapp.servlets;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.pentastagiu.holidayapp.model.Users;
import com.pentastagiu.holidayapp.service.LoginService;

@ManagedBean(name = "loginMB")
@ViewScoped
/**
 *
 * @author User
 */
public class LoginMB implements Serializable {

	private static final long serialVersionUID = 1L;
	@ManagedProperty("#{loginService}")
	private LoginService loginServ;
	private String password;
	private String message, uname;
	private Users currentUser;

	public LoginMB() {
		currentUser = new Users();
	}

	public void setCurrentUser(Users cUser) {
		this.currentUser = cUser;
	}

	public Users getCurrentUser() {
		return currentUser;
	}

	public void setLoginServ(LoginService loginServ) {
		this.loginServ = loginServ;
	}

	public LoginService getloginServ() {
		return this.loginServ;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String loginProject() {
		boolean result = loginServ.login(uname, password);
		if (result) {
			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().put("username", uname);
			return "home";
		} else {

			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_WARN,
							"Invalid Login!", "Please Try Again!"));

			// invalidate session, and redirect to other pages
			// message = "Invalid Login. Please Try Again!";
			return "login";
		}
	}

	public String logout() {
		ExternalContext context = FacesContext.getCurrentInstance()
				.getExternalContext();
		try {
			context.redirect("LoginMB.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
		FacesContext.getCurrentInstance().getExternalContext()
				.invalidateSession();
		return "logout";
	}

	public String displayUser() {
		HttpServletRequest req = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		HttpSession ses = req.getSession(false);
		String name = (String) ses.getAttribute("username");
		this.currentUser = loginServ.getUser(name);
		return name;
	}
}

package com.pentastagiu.holidayapp.servlets;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.view.ViewScoped;

import com.pentastagiu.holidayapp.model.Department;
import com.pentastagiu.holidayapp.service.DepartmentService;
import com.pentastagiu.holidayapp.service.EmployeeService;
import com.pentastagiu.holidayapp.service.UsersService;

@ManagedBean(name = "recruitmentMB")
@ViewScoped
public class RecruitmentMB implements Serializable {

    private static final long serialVersionUID = 2974714974857565093L;
    @ManagedProperty("#{departmentService}")
    private DepartmentService depServ;
    @ManagedProperty("#{employeeService}")
    private EmployeeService empServ;
    @ManagedProperty("#{usersService}")
    private UsersService userServ;

    public String test() {
        return System.getProperty("user.dir");
    }

    public void recruit() {

        try {
            BufferedReader bfr = new BufferedReader(
                    new FileReader(
                            "D:\\workspace\\EmployeeApp\\pentalog_departments_10.txt"));
            String line = new String();
            while ((line = bfr.readLine()) != null) {

                String name = line.split(":")[0];
                String desc = line.split(":")[1];
                Department dept = new Department(name,desc,null);  
                depServ.createDepartment(dept);
            }
            bfr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DepartmentService getDepServ() {
        return depServ;
    }

    public void setDepServ(DepartmentService depServ) {
        this.depServ = depServ;
    }

    public EmployeeService getEmpServ() {
        return empServ;
    }

    public void setEmpServ(EmployeeService empServ) {
        this.empServ = empServ;
    }

    public UsersService getUserServ() {
        return userServ;
    }

    public void setUserServ(UsersService userServ) {
        this.userServ = userServ;
    }

}

/*package com.pentastagiu.holidayapp.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pentastagiu.holidayapp.model.Department;
import com.pentastagiu.holidayapp.model.Employee;
import com.pentastagiu.holidayapp.service.DepartmentService;
import com.pentastagiu.holidayapp.service.EmployeeService;
import com.pentastagiu.holidayapp.service.HolidayService;
import com.pentastagiu.holidayapp.util.WebUtil;
import com.pentastagiu.holidayapp.view.DepartmentsView;
import com.pentastagiu.holidayapp.view.EmployeeView;
import com.pentastagiu.holidayapp.view.SearchView;

*//**
 * Creates an HTTP servlet suitable for a Web site
 *//*
public class SearchServlet extends HttpServlet {
    private static final long serialVersionUID = 744601559225832983L;
    @Inject
    public SearchView searchView;
    @Inject
    public EmployeeService emplServ;
    @Inject
    public EmployeeView empView;
    @Inject
    public DepartmentService depServ;
    @Inject
    public DepartmentsView depView;
    @Inject
    public HolidayService holServ;
    @Inject
    public HolidayView holView;

    *//**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     *//*
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        doPost(request, response);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        WebUtil webUtil = new WebUtil();
        ResourceBundle messages = webUtil.setLanguage(request);
        out.print(webUtil.createHtmlHeader("Search"));
        out.print(webUtil.createMenu(messages));

        out.print(searchView.createSearchMenu(messages));
    }

    *//**
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     *//*
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        WebUtil webUtil = new WebUtil();
        ResourceBundle messages = webUtil.setLanguage(request);
        String textbox = request.getParameter("textbox");
        if (textbox != null) {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            searchEmployee(request, textbox, webUtil, out, messages);
            searchDepartment(request, textbox, webUtil, out, messages);
            searchHoliday(request, textbox, webUtil, out, messages);

        }
    }

    *//**
     * Search employee
     *//*
    public void searchEmployee(HttpServletRequest request, String textbox,
            WebUtil webUtil, PrintWriter out, ResourceBundle messages)
            throws IOException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String firstName = request.getParameter("firstname");
        if (firstName != null) {
            firstName = textbox;
        }
        String lastName = request.getParameter("lastname");
        if (lastName != null) {
            lastName = textbox;
        }
        String manager = request.getParameter("manager");
        if (manager != null) {
            manager = textbox;
        }
        if (firstName != null || lastName != null || manager != null) {
            out.print(webUtil.getTableHead("center", 1));
            out.print(webUtil.tableHeaderEmployee(messages));

            List<Employee> employeeList = emplServ.getAllEmployees();
            int i = 0;
            for (Iterator<Employee> iterator = employeeList.iterator(); iterator
                    .hasNext();) {
                Employee currentEmployee = iterator.next();
                if (firstName != null) {
                    if (firstName.equals(currentEmployee.getManager()
                            .getFirstName())) {
                        out.print(empView.getTableContents("center",
                                currentEmployee, i++));
                    }
                }
                if (lastName != null) {
                    if (lastName.equals(currentEmployee.getLastName())) {
                        out.print(empView.getTableContents("center",
                                currentEmployee, i++));
                    }
                }
                if (manager != null) {
                    if (manager.equals(currentEmployee.getManager())) {
                        out.print(empView.getTableContents("center",
                                currentEmployee, i++));
                    }
                }
            }
            out.print(webUtil.getHtmlFooter());
        }
    }

    *//**
     * Search department
     *//*
    public void searchDepartment(HttpServletRequest request, String textbox,
            WebUtil webUtil, PrintWriter out, ResourceBundle messages)
            throws IOException {
        String idDep = request.getParameter("idDep");
        int idDepart = 0;
        if (idDep != null) {
            idDepart = Integer.parseInt(textbox);
        }
        String nameDep = request.getParameter("nameDep");
        if (nameDep != null) {
            nameDep = textbox;
        }
        if (idDep != null || nameDep != null) {
            out.print(webUtil.getTableHead("center", 1));
            out.print(webUtil.tableHeaderDepartment(messages));
            List<Department> depList = depServ.getAllDepartments();
            int i = 0;
            for (Iterator<Department> iterator = depList.iterator(); iterator
                    .hasNext();) {
                Department currentDepartment = iterator.next();
                if (idDep != null) {
                    if (idDepart == currentDepartment.getId()) {
                        out.print(depView.getTableContents("center",
                                currentDepartment, i++));
                    }
                }
                if (nameDep != null) {
                    if (nameDep.equals(currentDepartment.getName())) {
                        out.print(depView.getTableContents("center",
                                currentDepartment, i++));
                    }
                }
            }
            out.print(webUtil.getHtmlFooter());
        }
    }

    *//**
     * Search holiday
     *//*
    public void searchHoliday(HttpServletRequest request, String textbox,
            WebUtil webUtil, PrintWriter out, ResourceBundle messages)
            throws IOException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String holidayStart = request.getParameter("holidayStart");
        Date startDate = null;
        if (holidayStart != null) {
            try {
                startDate = dateFormat.parse(textbox);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        String holidayEnd = request.getParameter("holidayEnd");
        Date endDate = null;
        if (holidayEnd != null) {
            try {
                endDate = dateFormat.parse(textbox);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        String requestDate = request.getParameter("requestDate");
        Date reqDate = null;
        if (requestDate != null) {
            try {
                reqDate = dateFormat.parse(textbox);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (holidayStart != null || holidayEnd != null || requestDate != null) {
            out.print(webUtil.getTableHead("center", 1));
            out.print(webUtil.tableHeaderHoliday(messages));
            List<Holiday> holList = holServ.getAllHolidays();
            int i = 0;
            for (Iterator<Holiday> iterator = holList.iterator(); iterator
                    .hasNext();) {
                Holiday currentHoliday = iterator.next();
                if (holidayStart != null) {
                    if (startDate.equals(currentHoliday.getHolidayStart())) {
                        out.print(holView.getTableContents("center",
                                currentHoliday, i++));
                    }
                }
                if (holidayEnd != null) {
                    if (endDate.equals(currentHoliday.getHolidayEnd())) {
                        out.print(holView.getTableContents("center",
                                currentHoliday, i++));
                    }
                }
                if (requestDate != null) {
                    if (reqDate.equals(currentHoliday.getRequestDate())) {
                        out.print(holView.getTableContents("center",
                                currentHoliday, i++));
                    }
                }
            }
            out.print(webUtil.getHtmlFooter());
        }
    }
}
*/
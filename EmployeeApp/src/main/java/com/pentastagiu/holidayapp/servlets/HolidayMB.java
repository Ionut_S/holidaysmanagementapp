package com.pentastagiu.holidayapp.servlets;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;

import com.pentastagiu.holidayapp.model.Employee;
import com.pentastagiu.holidayapp.model.Holiday;
import com.pentastagiu.holidayapp.model.Users;
import com.pentastagiu.holidayapp.service.EmployeeService;
import com.pentastagiu.holidayapp.service.HolidayService;
import com.pentastagiu.holidayapp.service.LoginService;

@ManagedBean(name = "holidayMB")
@ViewScoped
public class HolidayMB implements Serializable {

	private static final long serialVersionUID = 1L;
	@ManagedProperty("#{holidayService}")
	private HolidayService holidayServ;
	@ManagedProperty("#{employeeService}")
	private EmployeeService empServ;
	@ManagedProperty("#{LoginService}")
	private LoginService loginServ;
	@ManagedProperty(value = "#{param.holidayId}")
	private Integer id;
	private String request;
	private String approval;
	private String start;
	private String end;
	private String name;
	private Holiday holiday;
	private List<Employee> listEmployee;
	private List<Holiday> listHoliday;
	private Employee employee;
	private Employee employee2;
	private Holiday editHoliday;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public HolidayMB() {

		holiday = new Holiday();
		editHoliday = new Holiday();
	}

	@PostConstruct
	public void init() {
		listEmployee = empServ.getAllEmployees();
		listHoliday = holidayServ.getAllHolidays();

	}

	public List<Holiday> getUserHolidays(Users currentUser) {
		System.out
				.println("bla bla: " + currentUser.getUserName().substring(1));
		Employee currentEmpl = empServ.getEmployeeByName(listEmployee,
				currentUser.getUserName().substring(1));
		return holidayServ.getHolidayByEmpl(listHoliday, currentEmpl);
	}

	public void addHoliday() {
		SimpleDateFormat date = new SimpleDateFormat("yyyy/MM/dd");
		Date startDate = null, endDate = null, reqDate = null, approvalDate = null;
		employee = empServ.getEmployeeByName(listEmployee, name);
		try {
			startDate = date.parse(start);
			endDate = date.parse(end);
			reqDate = date.parse(request);
			approvalDate = date.parse(approval);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		holiday.setHolidayStart(startDate);
		holiday.setHolidayEnd(endDate);
		holiday.setApprovalDate(approvalDate);
		holiday.setRequestDate(reqDate);
		holiday.setEmployee(employee);
		holidayServ.addHolidayDao(holiday);
		ExternalContext context = FacesContext.getCurrentInstance()
				.getExternalContext();
		try {
			context.redirect("HolidayFacesServlet.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void reset() {
		RequestContext.getCurrentInstance().reset("form:panel");
	}

	public String edit() {
		this.editHoliday = holidayServ.getHolidayById(listHoliday, id);
		this.editHoliday.setId(id);
		start = editHoliday.convertTime(editHoliday.getHolidayStart());
		end = editHoliday.convertTime(editHoliday.getHolidayEnd());
		approval = editHoliday.convertTime(editHoliday.getApprovalDate());
		request = editHoliday.convertTime(editHoliday.getRequestDate());
		name = editHoliday.getEmployee().getFirstName();

		return "/EditHolidayFacesServlet.xhtml";
	}

	public void update() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date startDate = null, endDate = null, approvalDate = null, requestDate = null;
		try {
			startDate = dateFormat.parse(start);
			endDate = dateFormat.parse(end);
			approvalDate = dateFormat.parse(approval);
			requestDate = dateFormat.parse(request);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.editHoliday.setId(id);
		employee2 = empServ.getEmployeeByName(listEmployee, name);
		this.editHoliday.setEmployee(employee2);
		this.editHoliday.setHolidayStart(startDate);
		this.editHoliday.setHolidayEnd(endDate);
		this.editHoliday.setApprovalDate(approvalDate);
		this.editHoliday.setRequestDate(requestDate);
		holidayServ.updateHoliday(this.editHoliday);
		ExternalContext context = FacesContext.getCurrentInstance()
				.getExternalContext();
		try {
			context.redirect("HolidayFacesServlet.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Employee getEmployee2() {
		return employee2;
	}

	public void setEmployee2(Employee employee2) {
		this.employee2 = employee2;
	}

	public Holiday getEditHoliday() {
		return editHoliday;
	}

	public void setEditHoliday(Holiday editHoliday) {
		this.editHoliday = editHoliday;
	}

	public void onCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("Item Cancelled");
		FacesContext.getCurrentInstance().addMessage(null, msg);
		listHoliday.remove((Holiday) event.getObject());
	}

	public void saveAction() {

		// get all existing value but set "editable" to false
		for (Holiday currentHol : listHoliday) {
			currentHol.setEditable(false);
		}

	}

	public void editAction(Holiday holiday) {

		holiday.setEditable(true);

	}

	public List<Holiday> getAllHolidays() {
		return holidayServ.getAllHolidays();
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getApproval() {
		return approval;
	}

	public void setApproval(String approval) {
		this.approval = approval;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Holiday getHoliday() {
		return holiday;
	}

	public void setHoliday(Holiday holiday) {
		this.holiday = holiday;
	}

	public HolidayService getHolidayServ() {
		return this.holidayServ;
	}

	public void setHolidayServ(HolidayService holidayServ) {
		this.holidayServ = holidayServ;
	}

	public EmployeeService getEmpServ() {
		return this.empServ;
	}

	public void setEmpServ(EmployeeService empServ) {
		this.empServ = empServ;
	}

	public void setLoginServ(LoginService loginServ) {
		this.loginServ = loginServ;
	}

	public LoginService getLoginServ() {
		return this.loginServ;
	}
}

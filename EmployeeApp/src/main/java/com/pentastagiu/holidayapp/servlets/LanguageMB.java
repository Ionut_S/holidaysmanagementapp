package com.pentastagiu.holidayapp.servlets;

import java.io.Serializable;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "language")
@SessionScoped
public class LanguageMB implements Serializable {

    private static final long serialVersionUID = -8844992530494609033L;
    private Locale locale;

    @PostConstruct
    public void init() {
        locale = FacesContext.getCurrentInstance().getApplication()
                .getDefaultLocale();
    }

    public Locale getLocale() {
        return locale;
    }

    public String getLanguage() {

        return locale.getLanguage();
    }

    public void changeLanguage(String language) {
        locale = new Locale(language);

        FacesContext.getCurrentInstance().getViewRoot()
                .setLocale(new Locale(language));
    }
}

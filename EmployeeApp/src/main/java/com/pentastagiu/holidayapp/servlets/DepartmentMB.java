package com.pentastagiu.holidayapp.servlets;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;

import com.pentastagiu.holidayapp.model.Department;
import com.pentastagiu.holidayapp.model.Employee;
import com.pentastagiu.holidayapp.service.DepartmentService;

@ManagedBean(name = "departmentMB")
@ViewScoped
public class DepartmentMB implements Serializable {

	private static final long serialVersionUID = 1L;
	@ManagedProperty("#{departmentService}")
	private DepartmentService depServ;
	@ManagedProperty(value = "#{param.departmentId}")
	private Integer id;
	private String name;
	private String description;
	List<Department> listDepartment;
	Department department;
	Department editDep;
	private Department selectedDepartment;

	public Department getSelectedDepartment() {
		return selectedDepartment;
	}

	public void setSelectedDepartment(Department selectedDepartment) {
		this.selectedDepartment = selectedDepartment;
	}

	public DepartmentMB() {
		department = new Department();
		editDep = new Department();

	}

	@PostConstruct
	public void init() {
		listDepartment = depServ.getAllDepartments();

	}

	public void reset() {
		RequestContext.getCurrentInstance().reset("Add:panel");
	}

	public void addDepartment() {
		Department dep = new Department(name, description, null);
		depServ.createDepartment(dep);
		ExternalContext context = FacesContext.getCurrentInstance()
				.getExternalContext();
		try {
			context.redirect("DepartmentFacesServlet.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String edit() {
		this.editDep = depServ.getDepartmentById(listDepartment, id);
		this.editDep.setId(id);
		this.name = this.editDep.getName();
		this.description = this.editDep.getDescription();

		return "/EditDepartmentFacesServlet.xhtml";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Department getEditDep() {
		return editDep;
	}

	public void setEditDep(Department editDep) {
		this.editDep = editDep;
	}

	public void update() {
		this.editDep.setEmpls(null);
		this.editDep.setId(id);
		this.editDep.setName(name);
		this.editDep.setDescription(description);
		depServ.updateDepartement(this.editDep);
		ExternalContext context = FacesContext.getCurrentInstance()
				.getExternalContext();
		try {
			context.redirect("DepartmentFacesServlet.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void onCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("Item Cancelled");
		FacesContext.getCurrentInstance().addMessage(null, msg);
		listDepartment.remove((Employee) event.getObject());
	}

	public void saveAction() {

		// get all existing value but set "editable" to false
		for (Department currentDep : listDepartment) {
			currentDep.setEditable(false);
		}

	}

	public void editAction(Employee employee) {

		employee.setEditable(true);

	}

	public DepartmentService getDepServ() {
		return depServ;
	}

	public void setDepServ(DepartmentService depServ) {
		this.depServ = depServ;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Department> getListDepartment() {
		return listDepartment;
	}

	public void setListDepartment(List<Department> listDepartment) {
		this.listDepartment = listDepartment;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public List<Department> getAllDepartments() {
		return depServ.getAllDepartments();
	}

}

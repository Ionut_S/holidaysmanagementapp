package com.pentastagiu.holidayapp.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "userroles")
public class UserRoles {
	@Id
	@GeneratedValue
	private Integer id;
	@Column
	private String roleId;
	@OneToMany(mappedBy = "role")
	private List<Users> user;

	public UserRoles() {

	}

	public UserRoles(String roleId) {
		this.setRole(roleId);
		this.user = null;
	}

	public void setRole(String roleId) {
		this.roleId = roleId;
	}

	public String getRole() {
		return this.roleId;
	}
}

package com.pentastagiu.holidayapp.model;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The class corresponding to holiday table
 *
 */

@Entity
@Table(name = "holiday")
public class Holiday {
	@Id
	@Column(name = "idHol")
	@GeneratedValue
	private Integer id;
	@Column
	private Date requestDate;
	@Column
	private Date approvalDate;
	@Column
	private Date holidayStart;
	@Column
	private Date holidayEnd;
	@JoinColumn(name = "idEmpl")
	@OneToOne
	private Employee employee;
	private boolean editable;
    
    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

	public Holiday() {

	}

	public Holiday(Date holidayStart, Date holidayEnd, Date requestDate,
			Date approvalDate, Employee employee) {
		this.setHolidayStart(holidayStart);
		this.setHolidayEnd(holidayEnd);
		this.setRequestDate(requestDate);
		this.setApprovalDate(approvalDate);
		this.setEmployee(employee);
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public void setHolidayEnd(Date holidayEnd) {
		this.holidayEnd = holidayEnd;
	}

	public void setHolidayStart(Date startDate) {
		this.holidayStart = startDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public Date getHolidayStart() {
		return holidayStart;
	}

	public Date getHolidayEnd() {
		return holidayEnd;
	}

	public Employee getEmployee() {
		return employee;
	}

	public String convertTime(Date time) {
		Format format = new SimpleDateFormat("yyyy/MM/dd");
		return format.format(time);
	}
	

}

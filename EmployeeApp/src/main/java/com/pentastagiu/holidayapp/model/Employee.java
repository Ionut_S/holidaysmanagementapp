package com.pentastagiu.holidayapp.model;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The class corresponding the employee table
 *
 */
@Entity
@Table(name = "employee")
public class Employee {
	@Id
	@GeneratedValue
	private Integer id;
	@Column
	private String firstName;
	@Column
	private String lastName;
	@Column
	private Date hiredate;
	@JoinColumn(name = "idMng")
	@ManyToOne(fetch = FetchType.EAGER)
	private Employee manager;
	@JoinColumn(name = "idDep")
	@ManyToOne(fetch = FetchType.EAGER)
	private Department dept;
	@OneToOne(mappedBy = "employee")
	private Holiday holiday;
	@OneToMany(mappedBy = "manager")
	private List<Employee> empls;
	private boolean editable;
	@JoinColumn(name = "idUser")
	@OneToOne
	private Users user;
	private String state;
	private String cnp;

	public Employee() {
	}

	public Employee(String firstName, String lastName, String cnp,
			Date hiredate, Employee manager, Department dept) {
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setHiredate(hiredate);
		this.setManager(manager);
		this.setDept(dept);
		this.holiday = null;
		this.empls = null;
		this.setUser(null);
		this.setState("active");
		this.setCnp(cnp);
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getCnp() {
		return cnp;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public Users getUser() {
		return this.user;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getState() {
		return this.state;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public void setHoliday(Holiday holiday) {
		this.holiday = holiday;
	}

	public Holiday getHoliday() {
		return holiday;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Date getHiredate() {
		return hiredate;
	}

	public void setHiredate(Date hiredate) {
		this.hiredate = hiredate;
	}

	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	public void setDept(Department dept) {
		this.dept = dept;
	}

	public Department getDept() {
		return dept;
	}

	/*
	 * @Override public int hashCode() { final int prime = 31; int result = 1;
	 * result = prime * result + ((dept == null) ? 0 : dept.hashCode()); result
	 * = prime * result + ((empls == null) ? 0 : empls.hashCode()); result =
	 * prime * result + ((firstName == null) ? 0 : firstName.hashCode()); result
	 * = prime * result + ((hiredate == null) ? 0 : hiredate.hashCode()); result
	 * = prime * result + ((id == null) ? 0 : id.hashCode()); result = prime *
	 * result + ((lastName == null) ? 0 : lastName.hashCode()); result = prime *
	 * result + ((manager == null) ? 0 : manager.hashCode()); return result; }
	 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (dept == null) {
			if (other.dept != null)
				return false;
		} else if (!dept.equals(other.dept))
			return false;
		if (empls == null) {
			if (other.empls != null)
				return false;
		} else if (!empls.equals(other.empls))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		return true;
	}

	public String convertTime(Date time) {
		Format format = new SimpleDateFormat("yyyy/MM/dd");
		return format.format(time);
	}

}

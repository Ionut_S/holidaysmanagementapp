package com.pentastagiu.holidayapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The class corresponding the user table
 *
 */
@Entity
@Table(name = "users")
public class Users {
	@Id
	@GeneratedValue
	private Integer id;
	@Column
	private String userName;
	@Column
	private String token;
	@Column
	private String salt;
	@OneToOne(mappedBy = "user")
	private Employee empl;
	@JoinColumn(name = "idRole")
	@ManyToOne
	private UserRoles role;

	public Users() {

	}

	public Users(String userName, String token, String salt, UserRoles role) {
		this.setUserName(userName);
		this.setToken(token);
		this.setSalt(salt);
		this.setUserRole(role);
	}

	public boolean hasRole(String roleName) {
		if (role != null)
			return (role.getRole()).equals(roleName);
		return false;
	}

	public void setUserRole(UserRoles role) {
		this.role = role;
	}

	public UserRoles getUserRole() {
		return this.role;
	}

	public void setEmployee(Employee empl) {
		this.empl = empl;
	}

	public Employee getEmployee() {
		return this.empl;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUserName() {
		return userName;
	}

	public String getToken() {
		return token;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getSalt() {
		return salt;
	}
}

package com.pentastagiu.holidayapp.service;

import java.util.List;

import com.pentastagiu.holidayapp.dao.EmployeeDao;
import com.pentastagiu.holidayapp.model.Employee;

public interface EmployeeService {

	public EmployeeDao getEmployeeDao();

	/**
	 * Add a new Employee object in database
	 * 
	 * @param employee
	 */
	public Employee addEmployeeDao(Employee employee);

	/**
	 * @return a list with all employees from database
	 */
	public List<Employee> getAllEmployees();

	public List<Employee> getEmployeesByCount(int startPosition, int count);

	/**
	 * @param id
	 * @return an Employee object found by id
	 */
	public Employee findById(int id);

	/**
	 * Update an object in Employee table
	 * 
	 * @param emp
	 */
	public Employee updEmp(Employee emp);

	/**
	 * @param employeeList
	 * @param id
	 * @return the employee that matches to the given id
	 */
	public Employee getEmployeeById(List<Employee> employeeList, Integer id);

	/**
	 * @param emplList
	 * @param name
	 * @return the employee that matches to the given last name
	 */
	public Employee getEmployeeByName(List<Employee> emplList, String name);

	public void deleteById(int id);

	public List<Employee> getManagerList(List<Employee> employeeList);

}
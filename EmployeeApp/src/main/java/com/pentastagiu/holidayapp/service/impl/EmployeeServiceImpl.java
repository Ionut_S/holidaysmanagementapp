package com.pentastagiu.holidayapp.service.impl;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.pentastagiu.holidayapp.DBInitialiser;
import com.pentastagiu.holidayapp.dao.EmployeeDao;
import com.pentastagiu.holidayapp.model.Employee;
import com.pentastagiu.holidayapp.model.UserRoles;
import com.pentastagiu.holidayapp.model.Users;
import com.pentastagiu.holidayapp.service.EmployeeService;
import com.pentastagiu.holidayapp.service.RoleService;
import com.pentastagiu.holidayapp.service.UsersService;

/**
 * Different dao services for Employee table from database
 */

@Named(value = "employeeService")
public class EmployeeServiceImpl implements EmployeeService {

	@Inject
	private EmployeeDao dao;
	@Inject
	private UsersService addServ;
	@Inject
	private RoleService roleServ;

	public EmployeeDao getDao() {
		return dao;
	}

	public void setDao(EmployeeDao dao) {
		this.dao = dao;
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.EmployeeService#getEmployeeDao()
	 */
	@Override
	public EmployeeDao getEmployeeDao() {
		return dao;
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.EmployeeService#addEmployeeDao(com.pentastagiu.holidayapp.model.Employee)
	 */
	@Override
	public Employee addEmployeeDao(Employee employee) {
		String userName = (employee.getFirstName().substring(0, 1) + employee
				.getLastName()).toLowerCase();
		String password = userName + employee.getCnp().substring(3);
		MessageDigest md;
		String salt = "a1s2d3f4g5h6j7k8";
		String token = new String();
		try {
			md = MessageDigest.getInstance("MD5");
			md.update((password.concat(userName).concat(salt)).getBytes());
			byte[] mdBytes = md.digest();
			StringBuffer tokenBuffer = new StringBuffer();
			for (int i = 0; i < mdBytes.length; i++) {
				tokenBuffer.append(Integer.toString(
						(mdBytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			token = tokenBuffer.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		UserRoles role;
		role = roleServ.getRoleByRoleId("user");
		addServ.addUserDao(new Users(userName, token, "a1s2d3f4g5h6j7k8", role));
		return dao.create(employee);
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.EmployeeService#getAllEmployees()
	 */
	@Override
	public List<Employee> getAllEmployees() {
		return dao.getAllDaoEmployees();
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.EmployeeService#getEmployeesByCount(int,
	 *      int)
	 */
	@Override
	public List<Employee> getEmployeesByCount(int startPosition, int count) {
		return dao.getDaoEmployeesByCount(startPosition, count);
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.EmployeeService#findById(int)
	 */
	@Override
	public Employee findById(int id) {
		return dao.find(id);
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.EmployeeService#updEmp(com.pentastagiu.holidayapp.model.Employee)
	 */
	@Override
	public Employee updEmp(Employee emp) {
		return dao.update(emp);
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.EmployeeService#getEmployeeById(java.util.List,
	 *      java.lang.Integer)
	 */
	@Override
	public Employee getEmployeeById(List<Employee> employeeList, Integer id) {
		for (Iterator<Employee> iterator = employeeList.iterator(); iterator
				.hasNext();) {
			Employee currentEmployee = iterator.next();

			int currentId = currentEmployee.getId();
			if (currentId == id)
				return currentEmployee;
		}
		return null;

	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.EmployeeService#getEmployeeByName(java.util.List,
	 *      java.lang.String)
	 */
	@Override
	public Employee getEmployeeByName(List<Employee> emplList, String name) {
		for (Iterator<Employee> iterator = emplList.iterator(); iterator
				.hasNext();) {
			Employee currentEmpl = iterator.next();

			String currentLastName = currentEmpl.getLastName();
			if (currentLastName.equalsIgnoreCase(name))
				return currentEmpl;

		}
		return null;
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.EmployeeService#deleteById(int)
	 */
	@Override
	public void deleteById(int id) {
		dao.delete(id);
	}

	public List<Employee> getManagerList(List<Employee> employeeList) {
		List<Employee> managerList = new ArrayList<>();
		for (Iterator<Employee> iterator1 = employeeList.iterator(); iterator1
				.hasNext();) {
			Employee currentEmployee = iterator1.next();
			String empName = currentEmployee.getFirstName();
			for (Iterator<Employee> iterator2 = employeeList.iterator(); iterator2
					.hasNext();) {
				Employee currentEmployee2 = iterator2.next();
				if (empName
						.equals(currentEmployee2.getManager().getFirstName())) {
					managerList.add(currentEmployee);
				}
			}

		}
		return managerList;
	}
}

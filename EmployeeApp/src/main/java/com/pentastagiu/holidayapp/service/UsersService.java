package com.pentastagiu.holidayapp.service;

import com.pentastagiu.holidayapp.model.Users;

public interface UsersService {
	/**
	 * Add a new User object in database
	 * 
	 * @param use
	 */
	public Users addUserDao(Users user);
}

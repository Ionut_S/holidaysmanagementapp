package com.pentastagiu.holidayapp.service;

import java.util.List;

import com.pentastagiu.holidayapp.dao.HolidayDao;
import com.pentastagiu.holidayapp.model.Employee;
import com.pentastagiu.holidayapp.model.Holiday;

public interface HolidayService {

	public HolidayDao getHolidayDao();

	/**
	 * Update an object in Holiday table
	 * 
	 * @param holiday
	 */
	public Holiday updateHoliday(Holiday holiday);

	/**
	 * @return a list with all holidays from database
	 */
	public List<Holiday> getAllHolidays();

	/**
	 * Add a new Holiday object in database
	 * 
	 * @param holiday
	 */
	public Holiday addHolidayDao(Holiday holiday);

	public List<Holiday> getHolidaysByCount(int startPosition, int count);

	/**
	 * @param holidayList
	 * @param id
	 * @return the holiday that matches to the given id
	 */
	public Holiday getHolidayById(List<Holiday> holidayList, Integer id);

	public Holiday findById(int id);

	public List<Holiday> getHolidayByEmpl(List<Holiday> holidayList,
			Employee empl);

}
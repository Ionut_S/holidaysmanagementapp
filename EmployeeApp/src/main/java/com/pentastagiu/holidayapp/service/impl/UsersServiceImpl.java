package com.pentastagiu.holidayapp.service.impl;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.pentastagiu.holidayapp.dao.UsersDao;
import com.pentastagiu.holidayapp.model.Users;
import com.pentastagiu.holidayapp.service.UsersService;

@Named(value = "usersService")
public class UsersServiceImpl implements UsersService {
	@Inject
	private UsersDao dao;

	/**
	 * @see com.pentastagiu.holidayapp.service.UsersService#addUserDao(com.pentastagiu.holidayapp.model.Users)
	 */
	@Override
	public Users addUserDao(Users user) {
		return dao.create(user);
	}

}

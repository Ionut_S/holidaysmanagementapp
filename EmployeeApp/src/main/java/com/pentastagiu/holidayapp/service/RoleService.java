package com.pentastagiu.holidayapp.service;

import com.pentastagiu.holidayapp.model.UserRoles;

public interface RoleService {
	public UserRoles getRoleByRoleId(String role);
}

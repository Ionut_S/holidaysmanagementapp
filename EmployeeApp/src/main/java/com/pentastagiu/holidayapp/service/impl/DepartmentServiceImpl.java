package com.pentastagiu.holidayapp.service.impl;

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.pentastagiu.holidayapp.dao.DepartmentsDao;
import com.pentastagiu.holidayapp.model.Department;
import com.pentastagiu.holidayapp.service.DepartmentService;

/**
 * Different dao services for Department table from database
 */

@Named(value = "departmentService")
public class DepartmentServiceImpl implements DepartmentService {
	@Inject
	private DepartmentsDao dao;

	public DepartmentsDao getDao() {
		return dao;
	}

	public void setDao(DepartmentsDao dao) {
		this.dao = dao;
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.DepartmentServiceImp#getDepartmentsDao()
	 */
	@Override
	public DepartmentsDao getDepartmentsDao() {
		return dao;
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.DepartmentServiceImp#getAllDepartments()
	 */
	@Override
	public List<Department> getAllDepartments() {
		return dao.getAllDepts();
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.DepartmentServiceImp#createDepartment(com.pentastagiu.holidayapp.model.Department)
	 */
	@Override
	public void createDepartment(Department dep) {
		dao.create(dep);
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.DepartmentServiceImp#updateDepartement(com.pentastagiu.holidayapp.model.Department)
	 */
	@Override
	public void updateDepartement(Department department) {
		dao.update(department);
	}

	/*
	 * @see
	 * com.pentastagiu.holidayapp.service.DepartmentServiceImp#getDepartmentsByCount
	 * (int, int)
	 */
	@Override
	public List<Department> getDepartmentsByCount(int startPosition, int count) {
		return dao.getDaoDepartmentsByCount(startPosition, count);
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.DepartmentServiceImp#getDepartmentById(java.util.List,
	 *      java.lang.Integer)
	 */
	@Override
	public Department getDepartmentById(List<Department> departList, Integer id) {
		for (Iterator<Department> iterator = departList.iterator(); iterator
				.hasNext();) {
			Department currentDep = iterator.next();

			Integer currentId = currentDep.getId();
			if (currentId == id)
				return currentDep;

		}
		return null;
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.DepartmentServiceImp#getDepartmentByName(java.util.List,
	 *      java.lang.String)
	 */
	@Override
	public Department getDepartmentByName(List<Department> departList,
			String name) {
		for (Iterator<Department> iterator = departList.iterator(); iterator
				.hasNext();) {
			Department currentDep = iterator.next();

			String currentName = currentDep.getName();
			if (currentName.equals(name))
				return currentDep;

		}
		return null;
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.DepartmentServiceImp#findById(int)
	 */
	@Override
	public Department findById(int id) {
		return dao.find(id);
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.DepartmentServiceImp#deleteById(int)
	 */
	@Override
	public void deleteById(int id) {
		dao.delete(id);
	}
}

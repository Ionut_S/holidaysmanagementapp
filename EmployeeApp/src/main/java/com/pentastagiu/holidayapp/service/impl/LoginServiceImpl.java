package com.pentastagiu.holidayapp.service.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.inject.Inject;
import javax.inject.Named;

import com.pentastagiu.holidayapp.dao.UsersDao;
import com.pentastagiu.holidayapp.model.Users;
import com.pentastagiu.holidayapp.service.LoginService;
import com.pentastagiu.holidayapp.DBInitialiser;
@Named(value = "loginService")
public class LoginServiceImpl implements LoginService {
	@Inject
	private UsersDao dao;

	public UsersDao getUsersDao() {
		return dao;
	}

	public void setUsersDao(UsersDao dao) {
		this.dao = dao;
	}

	@Override
	public Users getUser(String userName) {
		return dao.getUser(userName);
	}

	@Override
	public boolean login(String userName, String password) {
	    //DBInitialiser x=new DBInitialiser();
	    
	  /*  catch{
	    x.initiateUsers();
	    }*/
		// File file = new File("src/test/resources/file.txt");
		String salt = "a1s2d3f4g5h6j7k8";
		/*
		 * try { salt = (String) FileUtils.readLines(file).get(1); } catch
		 * (IOException e1) { e1.printStackTrace(); }
		 */
		Users u = getUser(userName);
		if (u.getUserName().equals(userName)) {
			MessageDigest md;
			String token = new String();
			try {
				md = MessageDigest.getInstance("MD5");
				md.update((password.concat(userName).concat(salt)).getBytes());
				byte[] mdBytes = md.digest();
				StringBuffer tokenBuffer = new StringBuffer();
				for (int i = 0; i < mdBytes.length; i++) {
					tokenBuffer.append(Integer.toString(
							(mdBytes[i] & 0xff) + 0x100, 16).substring(1));
				}
				token = tokenBuffer.toString();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			if (u.getToken().equals(token)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}

package com.pentastagiu.holidayapp.service;

import com.pentastagiu.holidayapp.model.Users;

public interface LoginService {
	/**
	 * @return the specified User by dao
	 */
	public Users getUser(String userName);

	public boolean login(String userName, String password);
}

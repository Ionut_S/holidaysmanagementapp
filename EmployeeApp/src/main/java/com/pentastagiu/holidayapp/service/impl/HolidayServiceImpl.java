package com.pentastagiu.holidayapp.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.pentastagiu.holidayapp.dao.HolidayDao;
import com.pentastagiu.holidayapp.model.Employee;
import com.pentastagiu.holidayapp.model.Holiday;
import com.pentastagiu.holidayapp.service.EmployeeService;
import com.pentastagiu.holidayapp.service.HolidayService;

/**
 * Different dao services for Holiday table from database
 */

@Named(value = "holidayService")
public class HolidayServiceImpl implements HolidayService {
	@Inject
	private HolidayDao dao;
	@Inject
	private EmployeeService empServ;

	public HolidayDao getDao() {
		return dao;
	}

	public void setDao(HolidayDao dao) {
		this.dao = dao;
	}

	public EmployeeService getEmpServ() {
		return empServ;
	}

	public void setEmpServ(EmployeeService empServ) {
		this.empServ = empServ;
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.HolidayService#getHolidayDao()
	 */
	@Override
	public HolidayDao getHolidayDao() {
		return dao;
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.HolidayService#updateHoliday(com.pentastagiu.holidayapp.model.Holiday)
	 */
	@Override
	public Holiday updateHoliday(Holiday holiday) {
		return dao.update(holiday);

	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.HolidayService#getAllHolidays()
	 */
	@Override
	public List<Holiday> getAllHolidays() {
		return dao.getAllHolidays();
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.HolidayService#addHolidayDao(com.pentastagiu.holidayapp.model.Holiday)
	 */
	@Override
	public Holiday addHolidayDao(Holiday holiday) {
		return dao.create(holiday);
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.HolidayService#getHolidaysByCount(int,
	 *      int)
	 */
	@Override
	public List<Holiday> getHolidaysByCount(int startPosition, int count) {
		return dao.getDaoHolidaysByCount(startPosition, count);
	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.HolidayService#getHolidayById(java.util.List,
	 *      java.lang.Integer)
	 */
	@Override
	public Holiday getHolidayById(List<Holiday> holidayList, Integer id) {
		for (Iterator<Holiday> iterator = holidayList.iterator(); iterator
				.hasNext();) {
			Holiday currentHoliday = iterator.next();

			int currentId = currentHoliday.getId();
			if (currentId == id)
				return currentHoliday;
		}
		return null;

	}

	@Override
	public List<Holiday> getHolidayByEmpl(List<Holiday> holidayList,
			Employee empl) {
		List<Holiday> holList = new ArrayList<Holiday>();
		for (Iterator<Holiday> iterator = holidayList.iterator(); iterator
				.hasNext();) {
			Holiday currentHoliday = iterator.next();

			Employee currentEmpl = currentHoliday.getEmployee();
			
			if (currentEmpl.getLastName().equals(empl.getLastName()))
				holList.add(currentHoliday);
		}
		return holList;

	}

	/**
	 * @see com.pentastagiu.holidayapp.service.impl.HolidayService#findById(int)
	 */
	@Override
	public Holiday findById(int id) {
		Holiday holiday = dao.getHolidayById(id);
		return holiday;

	}
}

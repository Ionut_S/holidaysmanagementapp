package com.pentastagiu.holidayapp.service;

import java.util.List;

import com.pentastagiu.holidayapp.dao.DepartmentsDao;
import com.pentastagiu.holidayapp.model.Department;

public interface DepartmentService {

    public DepartmentsDao getDepartmentsDao();

    /**
     * @return a list with all departments from database
     */
    public  List<Department> getAllDepartments();

    /**
     * Add a new Department object in database
     * 
     * @param dep
     */
    public  void createDepartment(Department dep);

    /**
     * Update an object in Department table
     * 
     * @param department
     */
    public  void updateDepartement(Department department);

    public  List<Department> getDepartmentsByCount(int startPosition,
            int count);

    /**
     * @param departList
     * @param id
     * @return the department that matches to the given id
     */
    public  Department getDepartmentById(List<Department> departList,
            Integer id);

    /**
     * @param departList
     * @param name
     * @return the department that matches to the given name
     */
    public  Department getDepartmentByName(List<Department> departList,
            String name);

    public  Department findById(int id);

    public  void deleteById(int id);

}
package com.pentastagiu.holidayapp.service.impl;

import javax.inject.Inject;
import javax.inject.Named;

import com.pentastagiu.holidayapp.dao.RoleDao;
import com.pentastagiu.holidayapp.model.UserRoles;
import com.pentastagiu.holidayapp.service.RoleService;

@Named(value = "RoleService")
public class RoleServiceImpl implements RoleService {
	@Inject
	private RoleDao dao;

	@Override
	public UserRoles getRoleByRoleId(String role) {
		return dao.getRoleByRoleId(role);
	}

}

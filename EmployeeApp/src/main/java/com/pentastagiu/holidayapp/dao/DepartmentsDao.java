package com.pentastagiu.holidayapp.dao;

import java.util.List;

import com.pentastagiu.holidayapp.model.Department;

/**
 * DepartmentDao extended from GenericDao
 */
public interface DepartmentsDao extends GenericDao<Department> {
	/**
	 * @param id
	 * @return a Department object that matches the id given
	 */
	public Department getDeptById(Integer id);

	/**
	 * @return a Department list from database with all Department objects
	 */
	public List<Department> getAllDepts();

	public List<Department> getDaoDepartmentsByCount(int startPosition,
			int count);

}

package com.pentastagiu.holidayapp.dao.impl;

import java.util.List;

import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.pentastagiu.holidayapp.dao.DepartmentsDao;
import com.pentastagiu.holidayapp.model.Department;

/**
 * DepartmentsDao implementation
 */
@Named(value = "departmentsDao")
public class DepartmentsDaoImpl extends GenericDaoImpl<Department> implements
		DepartmentsDao {

	/**
	 * @see com.pentastagiu.holidayapp.dao.DepartmentsDao#getDeptById(java.lang.Integer
	 *      )
	 */
	public Department getDeptById(Integer id) {
		Query query = this.em
				.createQuery("select u FROM Department u where u.id= :id");
		query.setParameter("id", id);
		@SuppressWarnings("unchecked")
		List<Department> users = query.getResultList();
		if (users != null && users.size() == 1) {
			return users.get(0);
		}
		return null;
	}

	/**
	 * @see com.pentastagiu.holidayapp.dao.DepartmentsDao#getAllDepts()
	 */
	public List<Department> getAllDepts() {
		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery<Department> q = cb.createQuery(Department.class);
		Root<Department> c = q.from(Department.class);
		q.select(c);
		TypedQuery<Department> query = em.createQuery(q);
		List<Department> results = query.getResultList();

		return results;

	}

	public List<Department> getDaoDepartmentsByCount(int startPosition,
			int count) {
		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery<Department> q = cb.createQuery(Department.class);
		Root<Department> c = q.from(Department.class);
		q.select(c);
		TypedQuery<Department> query = em.createQuery(q);
		query.setFirstResult(startPosition);
		query.setMaxResults(count);
		List<Department> results = query.getResultList();

		return results;

	}
}

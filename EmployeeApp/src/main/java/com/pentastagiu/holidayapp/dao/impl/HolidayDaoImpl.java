package com.pentastagiu.holidayapp.dao.impl;

import java.util.List;

import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.pentastagiu.holidayapp.dao.HolidayDao;
import com.pentastagiu.holidayapp.model.Holiday;

/**
 * HolidayDao implementation
 */
@Named(value = "holidayDao")
public class HolidayDaoImpl extends GenericDaoImpl<Holiday> implements
		HolidayDao {
	/**
	 * @see com.pentastagiu.holidayapp.dao.HolidayDao#getHolidayById(java.lang.Integer)
	 */
	public Holiday getHolidayById(Integer id) {
		Query query = this.em
				.createQuery("select u FROM Holiday u where u.id= :id");
		query.setParameter("id", id);
		@SuppressWarnings("unchecked")
		List<Holiday> holidays = query.getResultList();
		if (holidays != null && holidays.size() == 1) {
			return holidays.get(0);
		}
		return null;
	}

	/**
	 * @see com.pentastagiu.holidayapp.dao.HolidayDao#getAllHolidays()
	 */
	public List<Holiday> getAllHolidays() {
		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery<Holiday> q = cb.createQuery(Holiday.class);
		Root<Holiday> c = q.from(Holiday.class);
		q.select(c);
		TypedQuery<Holiday> query = em.createQuery(q);
		List<Holiday> results = query.getResultList();

		return results;
	}

	public List<Holiday> getDaoHolidaysByCount(int startPosition, int count) {
		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery<Holiday> q = cb.createQuery(Holiday.class);
		Root<Holiday> c = q.from(Holiday.class);
		q.select(c);
		TypedQuery<Holiday> query = em.createQuery(q);
		query.setFirstResult(startPosition);
		query.setMaxResults(count);
		List<Holiday> results = query.getResultList();

		return results;

	}

}

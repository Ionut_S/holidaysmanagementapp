package com.pentastagiu.holidayapp.dao.impl;

import java.util.List;

import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.pentastagiu.holidayapp.dao.UsersDao;
import com.pentastagiu.holidayapp.model.Users;

@Named(value = "usersDao")
public class UsersDaoImpl extends GenericDaoImpl<Users> implements UsersDao {

	/**
	 * @see com.pentastagiu.holidayapp.dao.UsersDao#getAllDaoUsers()
	 */
	@Override
	public List<Users> getAllDaoUsers() {
		em.clear();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Users> q = cb.createQuery(Users.class);
		Root<Users> c = q.from(Users.class);
		q.select(c);
		TypedQuery<Users> query = em.createQuery(q);
		List<Users> results = query.getResultList();
		return results;
	}

	@Override
	public Users getUser(String userName) {
		Query query = this.em
				.createQuery("select u FROM Users u where u.userName= :userName");
		query.setParameter("userName", userName);
		@SuppressWarnings("unchecked")
		List<Users> users = query.getResultList();
		return users.get(0);
	}
}

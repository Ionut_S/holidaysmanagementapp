package com.pentastagiu.holidayapp.dao;

/**
 * A generic dao based on JPA.
 */
public interface GenericDao<T> {
	/**
	 * Add the object in database
	 * 
	 * @param t
	 */
	T create(T t);

	/**
	 * Remove the specified object from database
	 * 
	 * @param id
	 */
	void delete(Object id);

	/**
	 * @param id
	 * @returns the corresponding object from database
	 */
	T find(Object id);

	/**
	 * Update the corresponding object in database with properties of the
	 * specified object
	 * 
	 * @param t
	 */
	T update(T t);
}

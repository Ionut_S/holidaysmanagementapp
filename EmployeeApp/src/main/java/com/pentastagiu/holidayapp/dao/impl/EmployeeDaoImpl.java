package com.pentastagiu.holidayapp.dao.impl;

import java.util.List;

import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.pentastagiu.holidayapp.dao.EmployeeDao;
import com.pentastagiu.holidayapp.model.Employee;

/**
 * EmployeeDao implementation
 */
@Named(value = "employeeDao")
public class EmployeeDaoImpl extends GenericDaoImpl<Employee> implements
		EmployeeDao {

	/**
	 * @see com.pentastagiu.holidayapp.dao.EmployeeDao#getAllDaoEmployees()
	 */
	@Override
	public List<Employee> getAllDaoEmployees() {
		em.clear();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Employee> q = cb.createQuery(Employee.class);
		Root<Employee> c = q.from(Employee.class);
		q.select(c);
		TypedQuery<Employee> query = em.createQuery(q);
		List<Employee> results = query.getResultList();
		return results;

	}

	@Override
	public List<Employee> getDaoEmployeesByCount(int startPosition, int count) {

		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery<Employee> q = cb.createQuery(Employee.class);
		Root<Employee> c = q.from(Employee.class);
		q.select(c);
		TypedQuery<Employee> query = em.createQuery(q);
		query.setFirstResult(startPosition);
		query.setMaxResults(count);
		List<Employee> results = query.getResultList();

		return results;

	}
}

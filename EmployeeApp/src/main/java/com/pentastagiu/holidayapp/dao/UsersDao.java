package com.pentastagiu.holidayapp.dao;

import java.util.List;

import com.pentastagiu.holidayapp.model.Users;

public interface UsersDao extends GenericDao<Users> {
	/**
	 * @return an User list from database with all User objects
	 */
	public List<Users> getAllDaoUsers();
	/**
	 * @return the specified User
	 */
	public Users getUser(String userName);
}

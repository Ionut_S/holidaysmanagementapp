package com.pentastagiu.holidayapp.dao;

import java.util.List;

import com.pentastagiu.holidayapp.model.Holiday;

/**
 * HolidayDao extended from GenericDao
 */
public interface HolidayDao extends GenericDao<Holiday> {
	/**
	 * @param id
	 * @return a Holiday object that matches the id given
	 */
	public Holiday getHolidayById(Integer id);

	/**
	 * @return a Holiday list from database with all Holiday objects
	 */
	public List<Holiday> getAllHolidays();

	public List<Holiday> getDaoHolidaysByCount(int startPosition, int count);

}

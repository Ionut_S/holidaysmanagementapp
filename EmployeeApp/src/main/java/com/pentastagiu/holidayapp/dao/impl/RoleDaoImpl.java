package com.pentastagiu.holidayapp.dao.impl;

import java.util.List;

import javax.persistence.Query;

import com.pentastagiu.holidayapp.dao.RoleDao;
import com.pentastagiu.holidayapp.model.UserRoles;

public class RoleDaoImpl extends GenericDaoImpl<UserRoles> implements RoleDao {

	@Override
	public UserRoles getRoleByRoleId(String role) {
		Query query = this.em
				.createQuery("select u FROM UserRoles u where u.roleId= :role");
		query.setParameter("role", role);
		@SuppressWarnings("unchecked")
		List<UserRoles> userRole = query.getResultList();
		if (userRole != null) {
			return userRole.get(0);
		}
		return null;
	}

}

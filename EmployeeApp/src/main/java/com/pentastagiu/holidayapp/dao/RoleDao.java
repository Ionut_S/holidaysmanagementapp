package com.pentastagiu.holidayapp.dao;

import com.pentastagiu.holidayapp.model.UserRoles;

public interface RoleDao extends GenericDao<UserRoles> {
	public UserRoles getRoleByRoleId(String role);
}

package com.pentastagiu.holidayapp.dao;

import java.util.List;

import com.pentastagiu.holidayapp.model.Employee;

/**
 * EmployeeDao extended from GenericDao
 */
public interface EmployeeDao extends GenericDao<Employee> {
	/**
	 * @return an Employee list from database with all Employee objects
	 */
	public List<Employee> getAllDaoEmployees();

	public List<Employee> getDaoEmployeesByCount(int startPosition, int count);

}

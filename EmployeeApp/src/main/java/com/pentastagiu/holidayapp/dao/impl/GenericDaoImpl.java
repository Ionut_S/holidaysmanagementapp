package com.pentastagiu.holidayapp.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.SessionFactory;

import com.pentastagiu.holidayapp.dao.GenericDao;

/**
 * Generic dao implementation
 */
public abstract class GenericDaoImpl<T> implements GenericDao<T> {

	@PersistenceContext
	protected EntityManager em;

	private Class<T> type;

	@SuppressWarnings("unchecked")
	public GenericDaoImpl(){
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("employeeDb");
		em = emf.createEntityManager();
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class<T>) pt.getActualTypeArguments()[0];
		em.clear();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
	}

	/**
	 * @see com.pentastagiu.holidayapp.dao.GenericDao#create(java.lang.Object)
	 */
	public T create(final T t) {
	
	    em.clear();	
		this.em.persist(t);
		em.getTransaction().commit();
		return t;
	}

	/**
	 * @see com.pentastagiu.holidayapp.dao.GenericDao#delete(java.lang.Object)
	 */
	public void delete(final Object id) {
		em.clear();
		this.em.remove(this.em.getReference(type, id));
	}

	/**
	 * @see com.pentastagiu.holidayapp.dao.GenericDao#find(java.lang.Object)
	 */
	public T find(final Object id) {
		em.clear();
		T t = this.em.find(type, id);
		return t;

	}

	/**
	 * @see com.pentastagiu.holidayapp.dao.GenericDao#update(java.lang.Object)
	 */
	public T update(final T t) {
		em.clear();
		this.em.merge(t);
		em.getTransaction().commit();
		return t;
	}
}

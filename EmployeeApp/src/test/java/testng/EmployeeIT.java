package testng;

import java.sql.DriverManager;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.pentastagiu.holidayapp.model.Department;
import com.pentastagiu.holidayapp.model.Employee;

@Test
public class EmployeeIT {

	EntityManager em;
	EntityManagerFactory emf;

	Employee emp3;

	@BeforeClass
	public void setUp() throws Exception {
		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
			DriverManager.getConnection(
					"jdbc:derby:memory:C:/Users/deni/testDB;create=true")
					.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			emf = Persistence.createEntityManagerFactory("testDB");
			em = emf.createEntityManager();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// populate database

		// departments
		Department dep1 = new Department("IT", "InfoTech", null);
		Department dep2 = new Department("HR", "Human Resources", null);
		em.getTransaction().begin();
		em.persist(dep1);
		em.persist(dep2);
		em.getTransaction().commit();
		String cnp = "201234";
		// employees
		Date hireDate1 = new Date(2012 / 02 / 07);
		Employee emp1 = new Employee("Marius", "Condrea", cnp, hireDate1, null,
				em.getReference(Department.class, 1));

		Date hireDate2 = new Date(2013 / 02 / 07);
		Employee manag = em.find(Employee.class, 1);
		Employee emp2 = new Employee("Albert", "Garcia", cnp, hireDate2, manag,
				em.getReference(Department.class, 1));

		Date hireDate = new Date(2013 / 02 / 07);
		emp3 = new Employee("Nikola", "Tesla", cnp, hireDate, manag,
				em.getReference(Department.class, 1));

		em.getTransaction().begin();
		em.persist(emp1);
		em.persist(emp2);
		em.persist(emp3);
		em.getTransaction().commit();
	}

	@AfterClass
	public void tearDown() {
		em.close();
		emf.close();
	}

	public void ReadEmployeeTest() {
		Employee emp1 = em.find(Employee.class, 3);
		Assert.assertEquals(emp3, emp1);
	}

	public void CreateEmployeeTest() {
		Department dep = em.find(Department.class, 1);
		Date hireDate = new Date(2013 / 02 / 07);
		Employee manag = em.find(Employee.class, 2);
		Employee emp = new Employee("Livadariu", "Bogdan", "102342", hireDate,
				manag, dep);
		em.getTransaction().begin();
		em.persist(emp);
		em.getTransaction().commit();
		Assert.assertTrue(em.contains(emp));
	}

	public void UpdateEmployeeTest() {
		Employee employee = em.find(Employee.class, 1);
		employee.setLastName("Michel");
		em.clear();
		em.getTransaction().begin();
		em.merge(employee);
		em.getTransaction().commit();
		Assert.assertEquals(em.getReference(Employee.class, 1).getLastName(),
				"Michel");
	}

	public void DeleteEmpoyeeTest() {
		Employee empl = em.getReference(Employee.class, 4);
		em.remove(empl);
		Assert.assertFalse(em.contains(empl));
	}
}

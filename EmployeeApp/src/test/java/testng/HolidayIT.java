package testng;

import java.sql.DriverManager;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.pentastagiu.holidayapp.model.Department;
import com.pentastagiu.holidayapp.model.Employee;
import com.pentastagiu.holidayapp.model.Holiday;

@Test
public class HolidayIT {

	EntityManager em;
	EntityManagerFactory emf;

	Holiday hol3;

	@BeforeClass
	public void setUp() throws Exception {
		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
			DriverManager.getConnection(
					"jdbc:derby:memory:C:/Users/deni/testDB;create=true")
					.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			emf = Persistence.createEntityManagerFactory("testDB");
			em = emf.createEntityManager();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// populate database

		// departments
		Department dep1 = new Department("IT", "InfoTech", null);
		Department dep2 = new Department("HR", "Human Resources", null);
		em.getTransaction().begin();
		em.persist(dep1);
		em.persist(dep2);
		em.getTransaction().commit();

		// employees
		Department dep = em.find(Department.class, 1);
		Date hireDate1 = new Date(2012 / 02 / 07);
		Employee emp1 = new Employee("Marius", "Condrea", "201233", hireDate1,
				null, dep);

		Date hireDate2 = new Date(2013 / 02 / 07);
		Employee manag = em.find(Employee.class, 1);
		Employee emp2 = new Employee("Albert", "Garcia", "123453", hireDate2,
				manag, dep);

		Date hireDate = new Date(2013 / 02 / 07);
		Employee emp3 = new Employee("Nikola", "Tesla", "125643", hireDate,
				manag, dep);

		em.getTransaction().begin();
		em.persist(emp1);
		em.persist(emp2);
		em.persist(emp3);
		em.getTransaction().commit();

		// holidays
		Holiday hol1 = new Holiday(new Date(2012 / 01 / 04), new Date(
				2012 / 01 / 10), new Date(2011 / 12 / 26), new Date(
				2011 / 12 / 28), em.getReference(Employee.class, 2));
		Holiday hol2 = new Holiday(new Date(2012 / 01 / 24), new Date(
				2012 / 02 / 10), new Date(2011 / 12 / 26), new Date(
				2011 / 12 / 28), em.getReference(Employee.class, 3));
		hol3 = new Holiday(new Date(2012 / 01 / 24), new Date(2012 / 02 / 10),
				new Date(2011 / 12 / 26), new Date(2011 / 12 / 28),
				em.getReference(Employee.class, 1));
		em.getTransaction().begin();
		em.persist(hol1);
		em.persist(hol2);
		em.persist(hol3);
		em.getTransaction().commit();
	}

	@AfterClass
	public void tearDown() {
		em.close();
		emf.close();
	}

	public void ReadHolidayTest() {
		Holiday holDb = em.find(Holiday.class, 3);
		Assert.assertEquals(hol3, holDb);
	}

	public void CreateHolidayTest() {
		Holiday hol4 = new Holiday(new Date(2012 / 01 / 24), new Date(
				2012 / 02 / 10), new Date(2011 / 12 / 26), new Date(
				2011 / 12 / 28), em.getReference(Employee.class, 1));

		em.getTransaction().begin();
		em.persist(hol4);
		em.getTransaction().commit();
		Assert.assertTrue(em.contains(hol4));
	}

	public void UpdateHolidayTest() {
		Holiday hol = em.find(Holiday.class, 1);
		Date newDate = new Date(2012 / 10 / 12);
		hol.setApprovalDate(newDate);
		em.clear();
		em.getTransaction().begin();
		em.merge(hol);
		em.getTransaction().commit();
		Assert.assertEquals(
				em.getReference(Holiday.class, 1).getApprovalDate(), newDate);
	}

	public void DeleteHolidayTest() {
		Holiday hol = em.getReference(Holiday.class, 2);
		em.remove(hol);
		Assert.assertFalse(em.contains(hol));
	}
}

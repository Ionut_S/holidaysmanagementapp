package testng;

import java.sql.DriverManager;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.pentastagiu.holidayapp.model.Department;

@Test
public class DepartmentIT {

	EntityManager em;
	EntityManagerFactory emf;

	Department dep2;

	@BeforeClass
	public void setUp() throws Exception {
		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
			DriverManager.getConnection(
					"jdbc:derby:memory:C:/Users/deni/testDB;create=true")
					.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			emf = Persistence.createEntityManagerFactory("testDB");
			em = emf.createEntityManager();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// populate database

		// departments
		Department dep1 = new Department("IT", "InfoTech", null);
		dep2 = new Department("HR", "Human Resources", null);
		em.getTransaction().begin();
		em.persist(dep1);
		em.persist(dep2);
		em.getTransaction().commit();
	}

	@AfterClass
	public void tearDown() {
		em.close();
		emf.close();
	}

	public void ReadDepartmentTest() {
		Department depDb = em.find(Department.class, 2);
		Assert.assertEquals(dep2, depDb);
	}

	public void CreateDepartmentTest() {
		Department dep = new Department("MRO",
				"Maintenance, repair and operations ", null);
		em.getTransaction().begin();
		em.persist(dep);
		em.getTransaction().commit();
		Assert.assertTrue(em.contains(dep));
	}

	public void UpdateDepartmentTest() {
		Department dep = em.find(Department.class, 1);
		dep.setDescription("Description test");
		em.clear();
		em.getTransaction().begin();
		em.merge(dep);
		em.getTransaction().commit();
		Assert.assertEquals(em.getReference(Department.class, 1)
				.getDescription(), "Description test");
	}

	public void DeleteDepartmentTest() {
		Department dep = em.getReference(Department.class, 3);
		em.remove(dep);
		Assert.assertFalse(em.contains(dep));
	}
}
